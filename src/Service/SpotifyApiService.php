<?php

namespace Drupal\cyberduck_test\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class to handle connection to Spotify API.
 */
class SpotifyApiService {

  /**
   * Guzzle Http Client.
   *
   * @var GuzzleHttp\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->client = new Client();
  }

  /**
   * Get cell content.
   *
   * @return mixedvoid
   *   Authorisation response.
   */
  private function authorize() {
    try {
      $authorization = $this->client->request('POST', 'https://accounts.spotify.com/api/token', [
        'form_params' => [
            'grant_type' => 'client_credentials',
            'client_id' => 'fc8ade65a47542a692e2f220b8af68b9',
            'client_secret' => 'c9f40d6d2dfa4c68ad0bf65fef6f850b'
        ],
      ]);

      return $response = json_decode($authorization->getBody());
    }
    catch (GuzzleException $e) {
      return \Drupal::logger('spotify_client')->error($e);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getArtists(Int $limit) {

    $auth = $this->authorize();

    // Spotify API serarch seems to require a query string, you cant simply specify artist, unless i'm missing something?
    // Anyway, I have passed a genre for the purposes of this test.
    $genre = 'rock';

    try {
      $request = $this->client->request('GET', 'https://api.spotify.com/v1/search?q=' . $genre . '&type=artist&market=GB&limit=' . $limit, [
        'headers' => [
          'Authorization' => $auth->token_type . ' ' . $auth->access_token,
        ],
      ]);

      $result = json_decode($request->getBody());
    }
    catch (GuzzleException $e) {
      return \Drupal::logger('spotify_client')->error($e);
    }

    return $result->artists->items;
  }

}
