<?php

namespace Drupal\cyberduck_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\cyberduck_test\Service\SpotifyApiService;

/**
 * Define API controller class.
 */
class SpotifyApiController extends ControllerBase {

  /**
   * Guzzle Http Client.
   *
   * @var Drupal\cyberduck_test\Service\SpotifyApiService
   */
  protected $spotifyApiConnection;

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content(Int $limit) {
    $this->spotifyApiConnection = new SpotifyApiService();

    return $this->spotifyApiConnection->getArtists($limit);

  }

}
