<?php

namespace Drupal\cyberduck_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cyberduck_test\Controller\SpotifyApiController;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides Section navigation block, jump menu on mobile.
 *
 * @Block(
 *   id = "artist_display_block",
 *   admin_label = @Translation("Artist Display Block"),
 *   category = @Translation("Cyber Duck")
 * )
 */
class ArtistDisplayBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $controller_variable = new SpotifyApiController();
    $artists = $controller_variable->content($this->configuration['artist_quantity']);

    $uriOptions = [
      'attributes' => ['target' => '_blank'],
    ];
    foreach ($artists as $value) {
      $link = Link::fromTextAndUrl($value->name, Url::fromUri($value->external_urls->spotify, $uriOptions))->toString();
      $items[] = $link;
    }
    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['artist_quantity'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#title' => $this->t('Quantity'),
      '#description' => $this->t('How many artists do you want to display? (max 20)'),
      '#default_value' => isset($config['artist_quantity']) ? $config['artist_quantity'] : '',
      '#maxlength' => 2,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('artist_quantity') > '20') {
      $form_state->setErrorByName('artist_quantity', $this->t('Maximum of 20 artists!'));
    }
    return $form_state;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['artist_quantity'] = $values['artist_quantity'];
  }

}
